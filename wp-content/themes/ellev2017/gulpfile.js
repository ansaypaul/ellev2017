var gulp = require('gulp');
// Requires the gulp-sass plugin
var sass = require('gulp-sass');
var useref = require('gulp-useref');

gulp.task('hello', function() {
  console.log('Hello Zell');
});

gulp.task('sass', function(){
  return gulp.src('app/scss/styles.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('app/css'))
});

gulp.task('sass', function(){
	return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('useref', function(){
  return gulp.src('app/*.php')
    .pipe(useref())
    .pipe(gulp.dest('dist'))
});


var browserSync = require('browser-sync').create();
gulp.task('browserSync', function() {                          //EXPLICATION DE LA TÂCHE BROWSERSYNC :
    browserSync.init([                                       //Gentil browsersync, focalise-toi sur les fichiers :                                            // .php contenus dans le dossier site,
    ], {                                                        // ATTENTION, ON NE PEUT SPECIFIER QUE L'UNE DES OPTIONS SUIVANTES, DONC METTRE CELLE QUE L'ON NE VEUT PAS EN COMMENTAIRE :
       proxy: "http://192.168.2.177:8080/ellev2017/",                         //Sinon, balance le tout sur localhost(:8888 < MAC)/site, ADAPTER "site" ET METTRE LE NOM DU DOSSIER DANS LEQUEL WP EST INSTALLE
       injectChanges: true                                   //et injecte les changements css
    });
});

gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('app/scss/**/*.scss', ['sass']); 
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/*.php', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload); 
  // Other watchers
})

