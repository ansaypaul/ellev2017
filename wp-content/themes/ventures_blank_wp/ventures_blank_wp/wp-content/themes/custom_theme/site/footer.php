		</section>

		<footer class="footer">
			<div class="ad ad--splash" format="splash"></div>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => false, 'items_wrap' => '<ul class="menu menu--footer">%3$s</ul>', 'walker' => new Custom_Menu ) ); ?>
		</footer>

		<div class="popup__wrapper popup__wrapper--closed">
			<div class="popup popup--closed">
				<div class="popup__inner">
					<span class="popup__close icon icon-close close--js"></span>
					<iframe class="popup__iframe" height="265px"></iframe>
				</div>
			</div>
		</div>

<?php wp_footer(); ?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/show_ads.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/main.int.js"></script>
</body>
</html>
