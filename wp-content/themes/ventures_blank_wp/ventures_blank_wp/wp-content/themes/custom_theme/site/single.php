<?php get_header(); ?>

	<main class="main">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'templates/single' );

		endwhile;
		?>
		<?php 
		$i = 0; $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
		if( $related ) { ?>
			<div class="others">
				<h2 class="others__title">Meer lezen</h2>
				<?php
				foreach( $related as $post ) {
					setup_postdata($post);
					if($i % 2 === 0) echo "<div class='row'>";
					get_template_part( 'templates/content' );
					if($i % 2 !== 0) echo "</div>";
					$i++;
				}
				if(($i - 1) % 2 !== 0) echo "</div>";
				wp_reset_postdata(); ?>
			</div>
		<?php } ?>
	</main>

	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>
