<?php get_header(); ?>
	<main class="main">
		<?php
			$i = 1;
			$_categories = array("cat 1", "Familie", "Liefde", "Samenleven", "Gasten", "Beauty & Body", "Jouw Vraag", "Tests");
			$agendaId = get_cat_ID('Agenda');
			$exclude_ids = '';

			// LAST POST
			$last_post = new WP_Query( array('posts_per_page' => 1, 'category__not_in' => $agendaId));
			if( $last_post->have_posts() ) : 
				while( $last_post->have_posts() ) : $last_post->the_post();
						get_template_part( 'templates/content-first' );
						$exclude_ids .= $post->ID;
				endwhile;
			endif;

			?>
			
			<div class="sidebar__block ad ad--mobile mobile" format="mediumMobile"></div>
			
			<?php

			// LAST POST OF EACH CAT, REMOVING POST ALREADY DISPLAYED
			foreach ($_categories as $cat) {
				$cat_id = get_cat_ID($cat);
				$latest_cat_post = new WP_Query( array('posts_per_page' => 1, 'category__not_in' => $agendaId, 'category__in' => $cat_id, 'post__not_in' => array( $exclude_ids )));
				
				if( $latest_cat_post->have_posts() ) : 
					while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
							if($i % 2 !== 0) echo "<div class='row'>";
							get_template_part( 'templates/content' );
							$exclude_ids .= ',' . $post->ID;
							if($i % 2 === 0) echo "</div>";
						$i++;
					endwhile;
					if($i % 2 !== 0) echo "</div>";
				endif;
			}
		?>
	</main>

	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>
