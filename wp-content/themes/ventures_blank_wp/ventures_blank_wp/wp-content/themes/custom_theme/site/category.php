<?php get_header(); ?>

	<main class="main">
		<?php
			$i = 0;
			if( have_posts() ) : 
				while( have_posts() ) : the_post();
					if($i === 0) get_template_part( 'templates/category-first' );
					else {
						if($i % 2 !== 0) echo "<div class='row'>";
						get_template_part( 'templates/category' );
						if($i % 2 === 0) echo "</div>";
					}
					$i++;
				endwhile;
				if(($i - 1) % 2 !== 0) echo "</div>";
			endif;
		?>
		<?php getNavigation(); ?>
	</main>

	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>