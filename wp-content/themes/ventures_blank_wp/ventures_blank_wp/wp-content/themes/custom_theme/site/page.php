<?php get_header(); ?>

	<main class="main">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'templates/page' );

		endwhile;
		?>
	</main>

	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>

