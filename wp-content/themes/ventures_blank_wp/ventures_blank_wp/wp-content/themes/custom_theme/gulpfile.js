/*
//////// PHASE 001 > INSTALLATION DES DEPENDANCES
UNE FOIS : installer en global gulp et tous les plugins :
npm install --save-dev gulp-git gulp gulp-rename gulp-sass gulp-htmlhint gulp-csslint gulp-imagemin imagemin-pngquant gulp-notify gulp-cssnano gulp-uglify gulp-htmlmin gulp-autoprefixer gulp-plumber browser-sync -g


Atteindre votre dossier de travail via le terminal et lancer cette ligne pour linker les plugins installés en global:
npm link gulp gulp-rename gulp-git gulp-sass gulp-htmlhint gulp-csslint gulp-imagemin imagemin-pngquant gulp-notify gulp-cssnano gulp-uglify gulp-htmlmin gulp-autoprefixer gulp-plumber browser-sync
*/

//////// PHASE 002 > CHARGEMENT DES DEPENDANCES
var gulp         = require('gulp');               // Il faut Gulp... Bah oui hein !
var rename       = require('gulp-rename');        // Renommage des fichiers
var htmlmin      = require('gulp-htmlmin');       // Minification des HTML
var sass         = require('gulp-sass');          // Conversion des SCSS en CSS
var autoprefixer = require('gulp-autoprefixer');  // Ajout des prefixes des vendeurs pour le css
var cssnano      = require('gulp-cssnano');       // Minification des CSS
var uglify       = require('gulp-uglify');        // Minification des JS
var plumber      = require('gulp-plumber');       // Verification des erreurs dans le terminal
var notify       = require('gulp-notify');        // Belles erreurs
var browserSync  = require('browser-sync');       // Syncronisation de l'apercu
var imagemin     = require('gulp-imagemin');      // Compresse les images sans perte
var pngquant     = require('imagemin-pngquant');  // Requis par imagemin pour les png
var htmlhint     = require("gulp-htmlhint");      // Checke si le html est clean aux yeux du W3C
var csslint      = require('gulp-csslint');       // Checke si le css est clean aux yeux du W3C

//////// PHASE 003 > PARAMETRAGE DES DEPENDANCES
//pour plumber :
var plumberOpts = {errorHandler: notify.onError({
                    title: 'Gulp',
                    message: 'Error: <%= error.message %>'
  })
};
//pour minify-html :
var htmlminOpts = {
                    collapseWhitespace: true,                   //Enlève les espaces,
                    removeAttributeQuotes : false,              //Enlève les guillemets pour les attributs (html 5 ok seulement),
                    removeRedundantAttributes : true,           //Enlève les attributs dont la valeur égale celle par défaut,
                    removeEmptyAttributes: true,                //Enlève les attributs vides,
                    removeScriptTypeAttributes: true,           //Enlève l'attribut type="text/javascript" des balises scripts, (autres types préservés),
                    removeStyleLinkTypeAttributes: true         //Enlève l'attribut type="text/css" des balises styles et link, (autres types préservés),
                  };
//pour autoprefixer :
var autoprefixerOpts = {
                    browsers: ['last 5 versions', '> 1%']
                  };
//pour imagemin
var imageminOpts = {
                    progressive: true,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [pngquant()]
                  };
//* Pour plus de parametres, aller lire la doc des dependances sur le site de Gulp. Ca vaut la peine, il y a plein de parametres assez OSEF

//////// PHASE 004 > DESCRIPTION DES TÂCHES A EFFECTUER
//Tâche pour les fichiers php
gulp.task('php', function() {                                   //EXPLICATION DE LA TÂCHE PHP :
  return gulp.src('integration/*.php')                          //Va me chercher, dans le dossier integration, tous les fichiers terminant par ".php",
    .pipe(gulp.dest('site/'));                                   //et sauvegarde le tout dans le dossier src
});
//Tâche pour les fichiers html
gulp.task('html', function() {                                  //EXPLICATION DE LA TÂCHE HTML :
  return gulp.src('integration/*.html')                         //Va me chercher, dans le dossier integration, tous les fichiers terminant par ".html",
    .pipe(plumber(plumberOpts))                                 //dis-moi dans le terminal si ils contiennent des erreurs,
    .pipe(htmlhint())                                           //checke si le html est clean aux yeux du W3C,
    .pipe(htmlhint.reporter())                                  //si non, dis-le moi dans la console,
    .pipe(htmlmin(htmlminOpts))                                 //minifie-les,
    .pipe(gulp.dest('site/'));                                  //et sauvegarde le tout dans le dossier site
});
//Tâches pour les fichiers scss
gulp.task('css', function() {                                   //EXPLICATION DE LA TÂCHE SCSS :
  return gulp.src('integration/assets/scss/*.int.scss')                //Va me chercher, dans le dossier integration/scss, tous les fichiers terminant par ".int.scss",
    .pipe(plumber(plumberOpts))                                 //dis-moi dans le terminal si ils contiennent des erreurs,
		.pipe(sass())                                               //traduits les de sass à css,
    .pipe(autoprefixer(autoprefixerOpts))                       //ajoute-moi les prefixes de vendeurs,
//    .pipe(csslint())                                           //checke si le css est clean aux yeux du W3C, (pour les puristes, commenté car polue vite le terminal),
//    .pipe(csslint.reporter())                                  //si non, dis-le moi dans la console,
    .pipe(cssnano())                                            //minifie le CSS qui a ete genere,
    .pipe(rename(function(path){                                //remplace, dans le nom du fichier, ".int" par ".min",
      path.basename = path.basename.replace(".int", ".min");
    }))
    .pipe(gulp.dest('site/css/'));                               //et sauvegarde le tout dans le dossier site/css
});
//Tâche pour les fichiers javascript
gulp.task('js', function() {                                    //EXPLICATION DE LA TÂCHE JAVASCRIPT :
  return gulp.src('integration/assets/js/*.int.js')                    //Va me chercher, dansx le dossier integration/js, tous les fichiers terminant par ".int.js",
    .pipe(plumber(plumberOpts))                                 //dis-moi dans le terminal si ils contiennent des erreurs,
    .pipe(uglify())                                             //minifie le JS qui a ete genere,
    .pipe(rename(function(path){                                //remplace, dans le nom du fichier, ".int" par ".min",
      path.basename = path.basename.replace(".int", ".min");
    }))
    .pipe(gulp.dest('site/js/'));                               //et sauvegarde le tout dans le dossier site/js
});
//Tâche pour les fichiers images
gulp.task('img', () => {                                        //EXPLICATION DE LA TÂCHE IMAGES :
  return gulp.src('integration/assets/img/*')                   //Va me chercher tout ce que contient le dossier 'integration/assets/img'
    .pipe(imagemin(imageminOpts))                               //Compresse-les sans perte de qualité
    .pipe(gulp.dest('site/assets/img/'));                       //et sauvegarde le tout dans le dossier integration/assets/img
});
//Tâche de syncronisation des images
gulp.task('fontsSyncro', function() {
  return gulp.src('integration/assets/fonts/*')
    .pipe(gulp.dest('site/assets/fonts/'));
});
gulp.task('imgSyncro', function() {
  return gulp.src('integration/assets/img/*')
    .pipe(gulp.dest('site/assets/img/'));
});
gulp.task('vidsSyncro', function() {
  return gulp.src('integration/assets/video/*')
    .pipe(gulp.dest('site/assets/video/'));
});
//Tâche browsersync
gulp.task('browser-sync', function() {                          //EXPLICATION DE LA TÂCHE BROWSERSYNC :
    browserSync.init([                                          //Gentil browsersync, focalise-toi sur les fichiers :
      "site/assets/css/*.css",                                         // .css contenus dans le dossier site/css,
      "site/assets/js/*.js",                                           // .js contenus dans le dossier site/js,
      "site/*.html",                                            // .html contenus dans le dossier site,
      "site/*.php"                                              // .php contenus dans le dossier site,
    ], {                                                        // ATTENTION, ON NE PEUT SPECIFIER QUE L'UNE DES OPTIONS SUIVANTES, DONC METTRE CELLE QUE L'ON NE VEUT PAS EN COMMENTAIRE :
      //   server: {                                               //Dans le cas ou on est pas sur un serveur Apache,
      //       baseDir: "./site/"                                  //balance le résulat du dossier site sur localhost:3000
      // }
       proxy: "localhost:8888/ventures_blank_wp",                         //Sinon, balance le tout sur localhost(:8888 < MAC)/site, ADAPTER "site" ET METTRE LE NOM DU DOSSIER DANS LEQUEL WP EST INSTALLE
       injectChanges: true                                   //et injecte les changements css
    });
});

//////// PHASE 005 > CES TÂCHES SE LANCENT LORSQUE UN FICHIER EST MODIFIE :
gulp.task('watch', ['css', 'browser-sync'], function()
{
  gulp.task('default',['add']);                                       // Run gulp's default task 
  gulp.watch('integration/tools/imgSyncro.css', ['imgSyncro']);
  gulp.watch('integration/tools/fontsSyncro.css', ['fontsSyncro']);
  gulp.watch('integration/tools/audioSyncro.css', ['audioSyncro']);
  gulp.watch('integration/tools/vidsSyncro.css', ['vidsSyncro']);
  gulp.watch('integration/tools/assetsSyncro.css', ['imgSyncro','fontsSyncro','audioSyncro','vidsSyncro']);
  gulp.watch('integration/tools/imagecompressor.css', ['img']);       //Si le fichier imagecompressor est modifié dans 'integration'      > lance la tâche 'img'
  gulp.watch('integration/*.php', ['php']);                           //Si un fichier php             est modifié dans 'integration'      > lance la tâche 'php'
  gulp.watch('integration/*.html', ['html']);                         //Si un fichier html            est modifié dans 'integration'      > lance la tâche 'html'
  gulp.watch('integration/assets/scss/*.scss', ['css']);                     //Si un fichier scss            est modifié dans 'integration/scss' > lance la tâche 'css'
  gulp.watch('integration/assets/js/*.int.js', ['js']);                      //Si un fichier js              est modifié dans 'integration/js'   > lance la tâche 'js'
  gulp.watch('site/*.html').on('change', browserSync.reload);         //Si un fichier html            est modifié dans 'site'             > recharge completement la page
  gulp.watch('site/*.php').on('change', browserSync.reload);          //Si un fichier php             est modifié dans 'site'             > recharge completement la page
});
//////// PHASE 006 > LES TÂCHES QUI SE LANCENT PAR DEFAUT LORSQUE GULP SE LANCE :
gulp.task('default', ['watch']);
