<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel=icon href="<?php bloginfo('template_directory'); ?>/assets/img/favicon.png" sizes="57x57" type="image/png">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.min.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class("boxed"); ?>>
<!-- Insert Analytics -->
<header class="header">
	<?php getCookieBox(); ?>
	<nav class="nav nav--top">
		<div class="wrapper">
		<li class="menu__item mobile"><a href="#" onclick="showMenu(event)" class="link menu__link icon icon-menu">Menu</a></li>
		<?php wp_nav_menu( array( 'theme_location' => 'topheader-left', 'container' => false, 'items_wrap' => '<ul class="nav__menu menu left desktop">%3$s</ul>', 'walker' => new Custom_Menu ) ); ?>
		<a class="logo logo--mobile" href="<?php echo home_url() ?>"><?php bloginfo('name'); ?></a>
		<?php wp_nav_menu( array( 'theme_location' => 'topheader-right', 'container' => false, 'items_wrap' => '<ul class="nav__menu menu right desktop">%3$s</ul>', 'walker' => new Custom_Menu ) ); ?>
		</div>
	</nav>
	<?php get_search_form() ?>
	<div class="ad ad--leaderboard" format="leaderboard" close="true"></div>
	<nav class="nav nav--main wrapper">
		<?php wp_nav_menu( array( 'theme_location' => 'topheader-mobile', 'container' => false, 'items_wrap' => '<ul class="nav__menu menu menu--mobile"><div class="scroll">%3$s</div></ul>', 'walker' => new Custom_Menu ) ); ?>
		<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => false, 'items_wrap' => '<ul class="nav__menu menu desktop">%3$s</ul>', 'walker' => new Custom_Menu ) ); ?>
	</nav>
</header>

	<section class="content wrapper">