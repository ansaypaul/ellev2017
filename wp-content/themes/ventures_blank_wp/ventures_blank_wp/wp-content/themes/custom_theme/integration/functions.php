<?php

function register_menus() {
  register_nav_menu('topheader-mobile',__( 'Top Mobile' ));
  register_nav_menu('topheader-left',__( 'Top Left' ));
  register_nav_menu('topheader-right',__( 'Top Right' ));
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}

add_action( 'init', 'register_menus' );
add_action( 'after_setup_theme', 'add_image_sizes' );

function add_image_sizes() {
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'thumbnail', 360,  189, array( 'center', 'center' ));
  add_image_size( 'thumbnail-mobile', 549,  288, array( 'center', 'center' ));
  add_image_size( 'gallery', 315, 9999);
  add_image_size( 'p_big', 970,  508, array( 'center', 'center' ));
}

class Custom_Menu extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth ) {
	    // depth dependent classes
	    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
	    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
	    $classes = array( 'menu__sub' );
	    $class_names = implode( ' ', $classes );
	  
	    // build html
	    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output )
    {
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
	function start_el( &$output, $item, $depth, $args ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
      
        // depth dependent classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
       
        $isCurrent = ( in_array( 'current-menu-item', $classes ) || in_array( 'current-menu-parent', $classes ) || in_array( 'current_page_ancestor', $classes ) );

        $depth_classes = array(
            ( $depth == 0 ? 'menu__item' : 'menu__sub__item' ),
            ( $args->has_children ? 'menu__item--parent' : '' ),
        );

        if ( $isCurrent ) $depth_classes[] = $depth == 0 ? 'menu__item--current' : 'menu__sub__item--current';

        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
      
        // passed classes
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
      
        // build html
        $output .= $indent . '<li class="' . $depth_class_names . '">';
      
        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="link ' . ( $depth > 0 ? 'menu__sub__link' : 'menu__link' ) . ($item->classes[0] === 'icon' ? ' ' . $item->classes[0] . ' ' . $item->classes[1] : '') . ($item->classes[0] === 'logo' ? ' ' . $item->classes[0] : '') . '"';
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    function end_el(&$output, $item, $depth=0, $args=array()) {
        $output .= "</a></li>\n";
    }
}

function wpdocs_custom_excerpt_length( $length ) {
    return 17;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

class widget_cover extends WP_Widget {

    function widget_cover() {
        parent::WP_Widget(false, $name = 'Cover Widget', array('description' => ''));
    }

    function widget($args, $instance) { 
        extract( $args );
        $link0 = strip_tags($instance['link0']);
        $text1 = strip_tags($instance['text1']);
        $link1 = strip_tags($instance['link1']);
        $text2 = strip_tags($instance['text2']);
        $link2 = strip_tags($instance['link2']);
        ?>

           <!--  <div class="cover desktop">
            	<span class="cover__close icon icon-close" onclick="document.getElementsByClassName('cover')[0].classList.add('cover__hide')"></span>
            	<img class="cover__img" src="<?php echo $link0; ?>">
                <span class="cover__overlay">
                	<div class="cover__table">
                   		<div>
                   				<?php if($text2) { ?><a class="cover__button" target="_blank" href="<?php echo $link2; ?>"><?php echo $text2; ?></a><?php } ?>
                    			<?php if($text1) { ?><a class="cover__button" target="_blank" href="<?php echo $link1; ?>"><?php echo $text1; ?></a><?php } ?>
                		</div>
                	</div>
                </span>
            </div> -->

                <img class="coverBox__img" src="<?php echo $link0; ?>">
                <div class="coverBox__table">
                    <div>
                        <?php if($text2) { ?><a class="coverBox__button" target="_blank" href="<?php echo $link2; ?>"><?php echo $text2; ?></a><?php } ?>
                        <?php if($text1) { ?><a class="coverBox__button" target="_blank" href="<?php echo $link1; ?>"><?php echo $text1; ?></a><?php } ?>
                        <!-- <div class="coverBox__button coverBox__button--fb fb-like" data-width="100%" data-href="https://www.facebook.com/Psychologies-Magazine-Vlaamse-editie-142054325896302/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>-->
                        <span class="coverBox__band">Nu in de winkel</span>
                    </div>
                </div>

        <?php
     }

    function update($new_instance, $old_instance) {
        return $new_instance; 
    }

    function form($instance) {
        $link0 = strip_tags($instance['link0']);
        $text1 = strip_tags($instance['text1']);
        $link1 = strip_tags($instance['link1']);
        $text2 = strip_tags($instance['text2']);
        $link2 = strip_tags($instance['link2']);
        ?>
            <p>
                <label for="<?php echo $this->get_field_id('link0'); ?>">Cover URL: </label>
                <input class="widefat" id="<?php echo $this->get_field_id('link0'); ?>" name="<?php echo $this->get_field_name('link0'); ?>" type="text" value="<?php echo attribute_escape($link0); ?>" />
            </p>
             <p>
                <label for="<?php echo $this->get_field_id('text1'); ?>">Text 1: </label>
                <input class="widefat" id="<?php echo $this->get_field_id('text1'); ?>" name="<?php echo $this->get_field_name('text1'); ?>" type="text" value="<?php echo attribute_escape($text1); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('link1'); ?>">Link 1: </label>
                <input class="widefat" id="<?php echo $this->get_field_id('link1'); ?>" name="<?php echo $this->get_field_name('link1'); ?>" type="text" value="<?php echo attribute_escape($link1); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('text2'); ?>">Text 2: </label>
                <input class="widefat" id="<?php echo $this->get_field_id('text2'); ?>" name="<?php echo $this->get_field_name('text2'); ?>" type="text" value="<?php echo attribute_escape($text2); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('link2'); ?>">Link 2: </label>
                <input class="widefat" id="<?php echo $this->get_field_id('link2'); ?>" name="<?php echo $this->get_field_name('link2'); ?>" type="text" value="<?php echo attribute_escape($link2); ?>" />
            </p>

        <?php       
    }

}

function custom_widget_init() {

	register_sidebar( array(
		'name'          => 'pubcover_hook',
		'id'            => 'cover_1'
	) );
    register_sidebar( array(
        'name'          => 'header_widgets',
        'id'            => 'header_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'custom_widget_init' );
register_widget('widget_cover');

function getCookieBox() {
    if ($_COOKIE['displayCookieConsent'] !== 'y') { ?>
        <div class="box box--cookie"><p class="box__text">Bij de voortzetting van uw navigatiesysteem, accepteert u het gebruik van cookies. Deze laatste verzekeren u een goede werking van onze diensten.</p><a href="#cookies" class="box__link" rel="bookmark">Meer weten</a><a class="box__close icon icon-close" title="Fermer le message"></a></div>
    <?php 
    }
}

function getNavigation() {
    ?>
    <div class="navigation">
        <?php if (get_previous_posts_link()) { ?>
                <div class="navigation__link navigation__link--left"><?php previous_posts_link('<span class="navigation__icon icon icon-previous"></span>Vorige')?></div>
        <?php } ?>
        <?php if (get_next_posts_link()) { ?>
                <div class="navigation__link navigation__link--right"><?php next_posts_link('Volgende<span class="navigation__icon icon icon-next"></span>')?></div>
        <?php } ?>
    </div>
<?php 
}

$role = get_role('editor'); 
$role->add_cap('edit_theme_options');

function custom_admin_menu() {

    $user = new WP_User(get_current_user_id());     
    if (!empty( $user->roles) && is_array($user->roles)) {
        foreach ($user->roles as $role)
            $role = $role;
    }

    if($role == "editor") { 
       remove_submenu_page( 'themes.php', 'themes.php' );
       remove_submenu_page( 'themes.php', 'nav-menus.php' ); 
    }       
}

add_action('admin_menu', 'custom_admin_menu');

function my_own_footer(){
  wp_deregister_script( 'wp-embed' ); ?>
<script>
  var _gaq = [['_setAccount', 'UA-34870494-3'], ['_trackPageview']];
  (function(d, t) {
    var g = d.createElement(t),
        s = d.getElementsByTagName(t)[0];
    g.src = 'https://ssl.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g, s);
  }(document, 'script'));
</script>

  <script language=javascript>
    var url = document.location.href
        url = url.replace('http://www.psychologies.be/nl/', '')
        url = url.replace('_/', '')
        url = url.replace('.html', '')
        url = url.replace('.php', '')
        url = url.split("/")

    var count = url.length
    var keyword = 'other'

    if(count == 1) { keyword = url[0] }
    if(count == 2) { keyword = url[0] }
    if(count == 3) { keyword = url[0]+'/'+url[1] }

    var index = 'index?'
        index = url[0].toLowerCase().indexOf(index.toLowerCase());

    if(index > -1 ) { keyword = 'other' }
    </script>

    <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    var pp_gemius_identifier = 'zItArTwvK51.mEtZuZudOoaSnH5dkfAe9JwgF5u_Hej.z7';
    var pp_gemius_extraparameters = new Array( 'lan=NL', 'key='+ keyword );

    // lines below shouldn't be edited
    (function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');gt.setAttribute('defer','defer'); gt.src=l+'://gabe.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
    //--><!]]>
    </script>
<?php 
    global $post;
    if (strpos($post->post_content,'[gallery') !== false){
        add_action( 'wp_enqueue_scripts', setScriptGallery() );
    }
}

function setScriptGallery() {
     wp_enqueue_script( 'photoswipe', get_bloginfo('template_directory') . '/assets/js/photoswipe.min.js' );
     wp_enqueue_script( 'photoswipe-ui', get_bloginfo('template_directory') . '/assets/js/photoswipe-ui.js' );
     wp_enqueue_script( 'photoswipe-exe', get_bloginfo('template_directory') . '/assets/js/photoswipe.js' );
}

add_action( 'wp_footer', 'my_own_footer' );

add_filter( 'mce_buttons', 'my_add_next_page_button', 1, 2 ); // 1st row
 
function my_add_next_page_button( $buttons, $id ){
    if ( 'content' != $id )
        return $buttons;
    array_splice( $buttons, 13, 0, 'wp_page' );
    return $buttons;
}

function strip_shortcode_gallery( $content ) {
    preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );

    if ( ! empty( $matches ) ) {
        foreach ( $matches as $shortcode ) {
            if ( 'gallery' === $shortcode[2] ) {
                $pos = strpos( $content, $shortcode[0] );
                if( false !== $pos ) {
                    return substr_replace( $content, '', $pos, strlen( $shortcode[0] ) );
                }
            }
        }
    }

    return $content;
}

