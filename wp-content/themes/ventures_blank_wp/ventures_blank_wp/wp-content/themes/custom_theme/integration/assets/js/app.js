function showMenu(e) {
	e.preventDefault();
	menuMain = document.getElementsByClassName('nav--main')[0];
	body = document.body;
	body.classList.toggle('body--active');
	setTimeout(function() {
		menuMain.classList.toggle('nav--active');
	},10);  
}

function setVendor(el, property, value) {
  el.style["webkit" + property] = value;
  el.style["Moz" + property] = value;
  el.style["ms" + property] = value;
  el.style["O" + property] = value;
}

function resetCSS(array) {
	for (var i = 0; i < array.length; i++) {
		if(array[i]) array[i].style = "";
	}
}

function setCookie( e, t, n ) {
	var r = new Date(),
		i = new Date();
	i.setTime( r.getTime() + n * 24 * 60 * 60 * 1e3 );
	document.cookie = e + '=' + encodeURIComponent( t ) + ';expires=' + i.toGMTString() + ';domain=psychologies.be;path=/'; // CHANGE DOMAIN
}

function getCookie( e ) {
	var t = document.cookie,
		n, r, i;
	e = e + '=';
	for ( r = 0, c = t.length; r < c; r++ ) {
		i = r + e.length;
		if ( t.substring( r, i ) == e ) {
			cookEnd = t.indexOf( ';', i );
			if ( cookEnd == -1 ) {
				cookEnd = t.length;
			}
			return decodeURIComponent( t.substring( i, cookEnd ) );
		}
	}
	return null;
}

function removeCookieBox() {
	if ( getCookie( 'displayCookieConsent' ) !== 'y' ) {
		var cookieBox = document.getElementsByClassName('box--cookie')[0];
		cookieBox.addEventListener('click', function() {
			this.parentNode.removeChild(this);
			setCookie( 'displayCookieConsent', 'y', 365 );
		});
	}
}

function whichTransitionEvent(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
      'transition':'transitionend',
      'OTransition':'oTransitionEnd',
      'MozTransition':'transitionend',
      'WebkitTransition':'webkitTransitionEnd'
    }

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
}

function showSearch(el) {
	var searchBox = document.getElementsByClassName('searchBox')[0],
		searchBoxInput = document.getElementsByClassName('searchBox__input')[0];
	el.classList.toggle('icon-search');
	el.classList.toggle('icon-close');
	searchBox.classList.toggle('searchBox--active');
	if(searchBox.classList.contains('searchBox--active')) searchBoxInput.focus();
}

function Newsletter() {
	this.init();
}

Newsletter.prototype = {
	init: function() {
		this.popup = document.getElementsByClassName('popup')[0];
		this.iframe = document.getElementsByClassName('popup__iframe')[0];
		this.wrapper = document.getElementsByClassName('popup__wrapper')[0];
		this.closeButton = document.getElementsByClassName('popup__close')[0];
		this.openButton = document.getElementsByClassName('icon-mail')[0];

		this.iframe.src = this.openButton.href;
		this.closeFn = this.close.bind(this);
		this.openFn = this.open.bind(this);

		this.wrapper.addEventListener('click', this.closeFn);
		this.closeButton.addEventListener('click', this.closeFn);
		this.openButton.addEventListener('click', this.openFn);
	},
	close: function(e) {
		e.preventDefault();
		this.wrapper.classList.add('popup__wrapper--closed');
		this.popup.classList.add('popup--closed');
		return false;
	},
	open: function(e) {
		e.preventDefault();
		this.wrapper.classList.remove('popup__wrapper--closed');
		this.popup.classList.remove('popup--closed');
		return false;
	}
}

var scrollY = {},
	transitionEvent = whichTransitionEvent(),
	resizeEnd;

document.addEventListener('DOMContentLoaded',function(){

	var	search = document.getElementsByClassName('icon-search')[0],
		sidebar = document.getElementsByClassName('sidebar')[0],
		scroll = document.getElementsByClassName('scroll')[0];

	search.addEventListener("click", function(e){
		e.preventDefault();
		showSearch(this);
		return false;
	});

	transitionEvent && sidebar.addEventListener(transitionEvent, function() {
		setVendor(sidebar, 'Transition', 'none');
	});

	if(searchLink = document.getElementsByClassName('search__content')[0]) 
		searchLink.addEventListener('click', function(e) {
			e.preventDefault();
			showSearch(search);
			return false;
		});

	removeCookieBox();

	new Newsletter();

	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	if( window.canRunAds === undefined ){
		  antiADBLOCK = document.getElementsByClassName('adblock')[0];
		  p = document.createElement('p');
		  p.innerHTML = "You seem to blocking Google AdSense ads in your browser.";
		  antiADBLOCK.style.cssText = 'text-align:center;display:block !important';
		  antiADBLOCK.appendChild(p);
		} else {
	}
	
	});

				
function init() {
	var body = document.body,
		main = document.getElementsByClassName('main')[0],
		content = document.getElementsByClassName('content')[0],
		header = document.getElementsByClassName('header')[0],
		menuMain = document.getElementsByClassName('nav--main')[0],
		menuTop = document.getElementsByClassName('nav--top')[0],
		firstPost = document.getElementsByClassName('post--first')[0],
		leaderboard = document.getElementsByClassName('ad--leaderboard')[0],
		cookieBox = document.getElementsByClassName('box--cookie')[0],
		sidebar = document.getElementsByClassName('sidebar')[0],
		sidebarFixed = false;

	var isMobile = body.clientWidth <= 728,
		hasTakeover = body.classList.contains('theme-ad-takeover'),
		hasPadding = false,
		closeButton = leaderboard.getElementsByClassName('close--js')[0],
		closeEventAdded = false;

	var offsetValueNav = leaderboard.offsetTop + leaderboard.clientHeight,
		offsetValue = menuMain.offsetTop + menuMain.clientHeight,
		headerHeight = header.clientHeight,
		leaderboardHeight = leaderboard.clientHeight,
		contentHeight = main.clientHeight,
		sidebarHeight = sidebar.clientHeight;

	window.addEventListener("scroll", onscroll);

	onscroll = function() {
		if(!isMobile) {
			var scrollY = window.pageYOffset;

			    if(scrollY >= offsetValue) {
					// SIDEBAR SCROLL TRANSFORM
			    	if(!sidebarFixed) {
			    		sidebarFixed = true;
			        	setVendor(sidebar, 'Transition', 'transform .2s ease');
			        }
					// HEADER SCROLL TRANSFORM
			        if(!header.classList.contains('header--fixed')) {
			    		padding = headerHeight + 'px';
			    		if(hasTakeover) leaderboard.classList.add('ad--hidden');
			    		header.classList.add('header--fixed');
			    		header.style.top = - header.clientHeight + 'px';
			        	setVendor(menuMain, 'Transform', 'translate(0, '+ header.clientHeight +'px)');
			        	setVendor(menuTop, 'Transform', 'translate(0, '+ header.clientHeight +'px)');
			        	if(cookieBox) setVendor(cookieBox, 'Transform', 'translate(0, '+ header.clientHeight +'px)');
			    		body.style.paddingTop = padding;
			    		hasPadding = true;
			    	}
			        offsetTmp = leaderboardHeight > 0 ? scrollY - headerHeight + header.clientHeight : scrollY - 20;
			        if(offsetTmp + sidebarHeight <= contentHeight) setVendor(sidebar, 'Transform', 'translate(0, ' + offsetTmp + 'px)');
			    } else {
					// SIDEBAR SCROLL TRANSFORM
			    	if(sidebarFixed) {
			    		sidebarFixed = false;
			        	setVendor(sidebar, 'Transition', 'transform .2s ease');
			        }
					// HEADER SCROLL TRANSFORM
			        if(header.classList.contains('header--fixed')) {
			    		header.classList.remove('header--fixed');
			    		if(hasTakeover) leaderboard.classList.remove('ad--hidden');
			    		padding = !hasTakeover ? leaderboardHeight + 20 + 'px' : 0 + 'px';
			    		header.style.top = '';
			    		setVendor(menuMain, 'Transform', 'translate(0)');
			        	setVendor(menuTop, 'Transform', 'translate(0)');
			        	if(cookieBox) setVendor(cookieBox, 'Transform', 'translate(0)');
			    		body.style.paddingTop = padding;
			    		hasPadding = true;
			    	}
			        setVendor(sidebar, 'Transform', 'translate(0)');
			    }

			    // LEADERBOARD SCROLL FIXED
				if(leaderboardHeight > 0 && !hasTakeover)
					if (scrollY > offsetValueNav) {
						if(!leaderboard.classList.contains('ad--fixed')) {
							leaderboard.classList.add('ad--fixed');
					    	padding = leaderboardHeight + (hasTakeover ? 0 : 20) + 'px';
					    	body.style.paddingTop = padding;
					    	body.style.paddingBottom = padding;
			    			hasPadding = true;
			    			if(closeButton && !closeEventAdded) {
			    				closeEventAdded = true;
			    				closeButton.addEventListener('click', function() {
			    					leaderboard.style.display = "none";
			    				});
			    			}
						} 
				    } else if (scrollY <= offsetValueNav){
				    	if(leaderboard.classList.contains('ad--fixed')) {
				    		leaderboard.classList.remove('ad--fixed');
			    			leaderboard.removeAttribute('style');
				    		padding = headerHeight + 'px';
					    	body.style.paddingTop = padding;
					    	body.style.paddingBottom = padding;
			    			hasPadding = true;
				    	}
				    }

				// RESET ANYWAY
			    if(scrollY <= offsetValueNav && hasPadding) {
			    	body.style.paddingTop = '0px';
			    	body.style.paddingBottom = '0px';
			    	hasPadding = false;
			    }
			
		} else {
			padding = leaderboardHeight + (hasTakeover ? 0 : 20) + 'px';
			body.style.paddingBottom = padding;
		}
	}

	window.onresize = function(){
		clearTimeout(resizeEnd);
		resizeEnd = setTimeout(function(){
			resetCSS([body, header, menuMain, menuTop, cookieBox]);
			body.classList.remove('body--active');
			menuMain.classList.remove('nav--active');
			scroll = document.getElementsByClassName('scroll')[0];
			if (scroll.getElementsByClassName('menu__item').length * 51 > (window.innerHeight - 40))
			scroll.style.height = window.innerHeight - 40 + 'px';
			else scroll.style.height = "";
		}, 100);
	};
}