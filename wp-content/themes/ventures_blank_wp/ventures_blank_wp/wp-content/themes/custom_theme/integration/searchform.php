<div class="searchBox">
	<form role="search" method="get" id="searchform"
	    class="searchBox__form wrapper" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	    <input type="text" class="searchBox__input" placeholder="Zoeken..." value="<?php echo get_search_query(); ?>" name="s" id="s" />
	    <button type="submit" class="searchBox__submit icon icon-search" id="searchsubmit"></button>
	</form>
</div>