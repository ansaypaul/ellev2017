<?php get_header(); ?>

	<main class="main">
		<header class="post__header">
			<h2 class="post__title post__title--noImage">Oops, the page can't be found !</h2>
		</header>
		<div class="post__content">
			You should probably <a href="/">go back</a> or do a <a href="#" class="search__content">search</a> !
		</div>
	</main>

	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>