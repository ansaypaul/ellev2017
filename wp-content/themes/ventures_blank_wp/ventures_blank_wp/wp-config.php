<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ventures');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~sPjap7(Sib_P3BGma/R5p5(*=-KWi,#^~peV#z%W+e{QW?3M)r_N%nLm{IrzS~V');
define('SECURE_AUTH_KEY',  '<O-;k)[5&-AN2E5+HhXb(?8[r%>Q{^;E*[fKY{-/ :4B+q!wV~S@Q$)Tw!B{D!dS');
define('LOGGED_IN_KEY',    '595[__Tyz 4wqJi^NFI&b_r,>ys2w2A%WuF1^Ibe*r-a1Gg`jy/s^-q8h?9G;joy');
define('NONCE_KEY',        ',wKtu)>Hoql{!{xg;@3.mH=zCv.@uTSiI}]~yWSXOnp)Y;6Sfd.P]XPH<ci]2_lt');
define('AUTH_SALT',        'bl?j.~Hf~P[o+L 5Wi3c6s[+m`BC.k%+0WO|sArWP@TLEKH6KD4mzQsCg~B|?|ls');
define('SECURE_AUTH_SALT', '3<f=-,;*-^9.d1v-C+7pttypQ+.[w9;M-/k(;0ie5??_<5aP,:^Q6x0FLWAk]EvF');
define('LOGGED_IN_SALT',   'li(Yy3~L9!y|oWBlBtP(0xLB$i>y|Lt)Ra{?>:!vx#IYLBNO~YN>rDy6yQ4s2u>R');
define('NONCE_SALT',       'Ah+kpc?`G%WS*4o/HLB;xW]a5>>es[]4U!r+PU1<(khT=L#BPYH+Hr^^~UF@Wr75');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
