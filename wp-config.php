<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'elleclean');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fs(iielz#ck3`zV>?J!gg&,B,m}}h}3YG<f. 9^g&K46pr?MCv9Hs~R6Qp3H]BDO');
define('SECURE_AUTH_KEY',  ',3^u*dQYJXk,#550&70|Pqnj$:UoQ,XX=T[_HZ1WfbOv=H|*q&8JI<-SipIzt*xF');
define('LOGGED_IN_KEY',    'OVTetMOks=W_d;FJBd#x$,MJ[y86v?-^8$Qr*$}XIxjg9iL!,Y*!7)NefJ=0K&_b');
define('NONCE_KEY',        '5ndpU>E ~Zd_s>N7c4]`-0^4)j&AA!y&cEO``qh+!)n:>q3|0ppj{){<!X_#|+WQ');
define('AUTH_SALT',        'NxCLQ%Y3=Jq!zBn9*!UMzz 1^/2jyg`m9s?N`KR9p;2F4)7*i]q4c}eNKBZ@iOQ}');
define('SECURE_AUTH_SALT', '9oi>rNYfOM=|+H(:h+d)-M.R3yYL3^uwL_/f&$)/i0f+V,m}+Z8HVbvF<Y+L!BaF');
define('LOGGED_IN_SALT',   '(xJD)+otkmTj]1ULUQ9[,eE:[;7NEAt`=UIX9B@;9r2XiP R&pO,DB7w}8L55S(X');
define('NONCE_SALT',       'Sd^0P[GDL2:$GXQ07wc6[Y|EnwNM3x^%#tjB QNZ~:>(YdFpf0L@VTNwX5CQavB0');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');